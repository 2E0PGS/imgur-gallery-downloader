#!/bin/bash

# A script to download Imgur Gallerys into one folder.

# Copyright (C) <2015-2020>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo "Imgur Gallery Downloader"

if [ "$1" = "" ] || [ "$2" = "" ]; then
	echo "Usage: `basename $0` <Gallery Name> <Number of pages to get>"
	exit 1
fi

echo "Making new folder called: "$1
mkdir $1

echo "Changing into new directory"
cd $1
echo $PWD

for ((i = 0; i <= $2; i++)); do
	echo "Grabbing page $i"

	wget -q "https://imgur.com/r/$1/page/$i" -O - | grep 'class="post"' | cut -d\" -f2 | while read id
	do
		# Check picture size to prevewnt downloading of "removed" images.
		picture_length=$(wget "https://imgur.com/$id.jpg" -O- --spider -O- 2>&1 | grep 'Length' | cut -d' ' -f 2)
		# echo "Picture length: "$picture_length
		if grep -q '503' <<<$picture_length; then
			echo "Skipping Removed Image"
		else
			embed_video_raw_url=$(wget -q "https://imgur.com/$id" -O- | grep 'og:video' | cut -d\" -f4)
			if grep -q 'mp4' <<<$embed_video_raw_url; then
				echo "Downloading $id.mp4"
				wget -N -q -c "http://i.imgur.com/$id.mp4"
			else
				embed_raw_url=$(wget -q "https://imgur.com/$id" -O- | grep 'og:image' | cut -d\" -f4)
				if grep -q 'gif' <<<$embed_raw_url; then
					echo "Downloading $id.gif"
					wget -N -q -c "http://i.imgur.com/$id.gif"
				elif grep -q 'png' <<<$embed_raw_url; then
					echo "Downloading $id.png"
					wget -N -q -c "http://i.imgur.com/$id.png"
				else
					echo "Downloading $id.jpg"
					wget -N -q -c "http://i.imgur.com/$id.jpg"
				fi
			fi
		fi
	done
done

echo "Done"