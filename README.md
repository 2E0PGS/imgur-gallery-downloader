# Imgur Gallery Downloader

### Downloads a specified Imgur Gallery (for example: Wallpapers)

### Written in Shell script so it runs on Linux

### How to use it

* Clone Repo
* Make the file executable: `chmod +x imgur-gallery-downloader.sh`
* Run the program: `./imgur-gallery-downloader.sh <gallery name> <number of pages to get>`
* Win!

### Licence

```

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```